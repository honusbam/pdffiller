﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace pdfFiller
{
    public class worker
    {
        //https://manning-content.s3.amazonaws.com/download/d/3645210-8560-4e6d-9b03-3f9aca1921a5/samplechapter6.pdf
        public static byte[] fillForms(List<pdfForm> formList, bool includeBlankPageBetweenForms, Uri blankPageURL, bool flatten = false)
        {
            byte[] retData = null;

            if (formList != null && formList.Count > 0)
            {
                using (MemoryStream msOutput = new MemoryStream())
                {
                    Document doc = new Document();
                    PdfWriter pCopy = new PdfSmartCopy(doc, msOutput);
                    doc.Open();

                    for (int k = 0; k < formList.Count; k++)
                    {
                        PdfReader pdfFile = new PdfReader(formList[k].formData);
                        PdfReader.unethicalreading = true;  //allows for reading in of password-protected files

                        using (MemoryStream stampStream = new MemoryStream())
                        {
                            using (PdfStamper stamp = new PdfStamper(pdfFile, stampStream))
                            {
                                stamp.FormFlattening = flatten;
                                AcroFields formFields = stamp.AcroFields;

                                if (formFields != null && formFields.Fields != null && formFields.Fields.Count > 0)
                                {
                                    var fieldKeys = formFields.Fields.Keys;

                                    formFields.GenerateAppearances = true;  //this is required so that edits appear.  stupid

                                    List<pdfField> fieldList = formList[k].formFields;
                                    if (fieldList != null && fieldList.Count > 0)
                                    {
                                        foreach (pdfField field in fieldList)
                                        {
                                            if (!string.IsNullOrWhiteSpace(field.fieldData))
                                            {
                                                //there is an issue where the fieldKeys could have a "." at the end based on some previous actions, but we don't have that in the db
                                                //this check helps us find the key containing the expected field name
                                                string fieldKey = fieldKeys.FirstOrDefault(f => f.ToUpper().Replace(".", "").Equals(field.fieldName.ToUpper().Replace(".", "")));
                                                if (!string.IsNullOrWhiteSpace(fieldKey))
                                                {
                                                    formFields.SetField(fieldKey, field.fieldData);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            pdfFile = new PdfReader(stampStream.ToArray());
                        }

                        for (int i = 1; i < pdfFile.NumberOfPages + 1; i++)
                        {
                            ((PdfSmartCopy)pCopy).AddPage(pCopy.GetImportedPage(pdfFile, i));
                        }

                        if (includeBlankPageBetweenForms && k + 1 < formList.Count)
                        {
                            using (PdfReader blankFile = new PdfReader(blankPageURL))
                            {
                                ((PdfSmartCopy)pCopy).AddDocument(blankFile);
                            }
                        }

                        pCopy.FreeReader(pdfFile);
                        pdfFile.Close();
                    }

                    pCopy.Close();
                    doc.Close();
                    formList.Clear();

                    retData = msOutput.ToArray();
                    pCopy.Dispose();
                    doc.Dispose();
                }
            }

            return retData;
        }
    }
}